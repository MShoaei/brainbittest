﻿using Neuro.Native;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BrainbBitDemoConsole
{
    class Program
    {
        private Neuro.DeviceEnumerator deviceEnumerator;
        private Neuro.Device activeDevice;
        private ChannelInfoArray deviceChannels;
        private int batteryCharge;
        private List<Neuro.Native.ChannelInfo> channels;
        public Program()
        {

            deviceEnumerator = new Neuro.DeviceEnumerator(DeviceType.Brainbit);
            deviceEnumerator.DeviceListChanged += DeviceEnumerator_DeviceListChanged;
        }

        private void DeviceEnumerator_DeviceListChanged(object sender, EventArgs e)
        {
            Console.WriteLine("List changed");
            Console.WriteLine(sender.GetType());
            Console.WriteLine(e.GetType());

            foreach (DeviceInfo d in deviceEnumerator.Devices)
            {
                activeDevice = deviceEnumerator.CreateDevice(d);
                activeDevice.Connect();
                while (activeDevice.ReadParam<DeviceState>(Parameter.State) != DeviceState.Connected)
                {
                }
                
                string name = activeDevice.ReadParam<string>(Parameter.Name);
                DeviceState state = activeDevice.ReadParam<DeviceState>(Parameter.State);
                Console.WriteLine($"ReadParam: {{Name:{name}, State:{state}}}");
                Console.WriteLine($"Direct read: {{Name:{d.Name}, ID:{d.Id}}}");
            }
        }

        public void PrintBatteryCharge()
        {
            Neuro.BatteryChannel battery = new Neuro.BatteryChannel(activeDevice);
            Console.WriteLine($"[{0}]", battery.ReadData(0, battery.TotalLength));
        }

        public void ReadRawData()
        {
            IList<ChannelInfo> list = Neuro.DeviceTraits.GetChannelsWithType(activeDevice, ChannelType.Signal);
            
            List<Neuro.DataChannel<double>> v = new List<Neuro.DataChannel<double>>(activeDevice.Channels.Count());
            activeDevice.AddDoubleChannelDataListener(ReadLastData, list.First());
            
        }

        public static void ReadLastData(object sender, Neuro.Device.ChannelData<double> channelData)
        {
            Console.WriteLine($"ReadLastData: {channelData.ChannelInfo.Index}, {channelData.ChannelInfo.Name}");
            Console.WriteLine($"[{0}]", channelData.DataArray);
        }

        static void Main(string[] args)
        {
            Program p = new Program();
            p.PrintBatteryCharge();
            p.ReadRawData();
            Console.ReadLine();
        }
    }
}
